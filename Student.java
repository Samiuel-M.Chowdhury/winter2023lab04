
public class Student {

	private String programType;
	private String name;
	private int age;
	public int amountLearnt;
	
	public void predictGraduation(int age, String programType) {
		int yearsOfStudying;
		if(programType.equals("Technical")) {
			yearsOfStudying=3;
		}else {
			yearsOfStudying=2;
		}
		System.out.println("You will graduate by the age of "+(yearsOfStudying+age));
	}
	
	public void describStudentMajor(String name, String programType) {
		System.out.println(name +" majors in a "+ programType+" program.");
	}
	public void learn(int amountStudied){
		if(amountStudied>0){
			amountLearnt=amountStudied+amountLearnt;
		}
	}
	public String getProgramType(){
		return this.programType;
	}
	public void setProgramType(String newProgramType){
		this.programType=newProgramType;
	}
	public String getName(){
		return this.name;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public Student(String name, int age){
		this.programType="Pre-University";
		this.name=name;
		this.age=age;
		this.amountLearnt=0;
	}
}
