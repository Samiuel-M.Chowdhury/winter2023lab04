import java.util.Scanner;
public class Application {

	public static void main(String[] args) {
		Student Sam=new Student("Sam",17);
		//Before set
		System.out.println(Sam.getAge());
		Sam.setProgramType("Technical");
		System.out.println(Sam.getName());
		//After set
		System.out.println(Sam.getAge());
		System.out.println(Sam.getProgramType());
		Student Chow=new Student("Poly",20);
		Chow.setProgramType("Pre-University");
		System.out.println(Chow.getName());
		System.out.println(Chow.getAge());
		System.out.println(Chow.getProgramType());
		
		Sam.predictGraduation(Sam.getAge(), Sam.getProgramType());
		Chow.describStudentMajor(Chow.getName(), Chow.getProgramType());
		
		Student[] section3=new Student[3];
		section3[0]=Sam;
		section3[1]=Chow;
		System.out.println(section3[0].getName());
		section3[2] = new Student("John",30);
		System.out.println(section3[2].getProgramType());
		
		//New things added
		Scanner keyboard = new Scanner(System.in);
		int amountStudied=keyboard.nextInt();
		int amountStudiedNegativeNumber=keyboard.nextInt();
		System.out.println(section3[0].amountLearnt);
		
		// the 0 0 0 2 for part 2
		section3[2].learn(amountStudied);
		section3[2].learn(amountStudied);
		//Negative and positive block tester
		section3[1].learn(amountStudied);
		section3[2].learn(amountStudied);
		System.out.println(section3[0].amountLearnt);
		System.out.println(section3[1].amountLearnt);
		System.out.println(section3[2].amountLearnt);
		
		//Lab 4 part 2
		Student studentDefault=new Student("Samiuel",18);
		studentDefault.setProgramType("Technical");
		System.out.println(studentDefault.getProgramType());
		System.out.println(studentDefault.getName());
		System.out.println(studentDefault.getAge());
		System.out.println(studentDefault.amountLearnt);
	}

}
