public class Microwave{
	private int maxTemperature;
	private String brand;
	private String colour;
	public void printBrandAndColor(String brand, String colour){
		System.out.println("The brand of your microwave is from "+brand +" and the color is "+ colour);
	}
	public void printTemperatureWarning(int temperature){
		System.out.println("Your microwave should not exceed "+(temperature-60)+"°C at minimum");
	}
	public void printCountDown(int time){
		this.maxTemperature=time;
		if(time > 0){
			System.out.println("Your food will be ready in ");
			for(int i=time;i>0;i--){
				i=i/2;
				System.out.print(i+" seconds,");
			}
			System.out.println("DONE! Enjoy your meal.");
		}
	}
	private void helperCountDown(){
		System.out.println("Temperature below 0° for a microwave is absurd");
	}
	
	public int getMaxTemperature(){
		return this.maxTemperature;
	}
	public void setMaxTemperature(int newTemperature){
		this.maxTemperature=newTemperature;
	}
	public String getBrand(){
		return this.brand;
	}
	public void setBrand(String newBrand){
		this.brand=newBrand;
	}
	public String getColour(){
		return this.colour;
	}
	//HERE I DELETED MY SETTER FOR COLOUR
	public Microwave(int temperature,String brand, String colour){
		this.maxTemperature=temperature;
		this.brand=brand;
		this.colour=colour;
	}
}